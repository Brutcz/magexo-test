import { renderSuspended } from "@nuxt/test-utils/runtime";
import { expect, test } from "vitest";
import IndexPage from "~/pages/index.vue";

test("can render index page", async () => {
  const component = await renderSuspended(IndexPage);
  expect(component).toBeDefined();
});
