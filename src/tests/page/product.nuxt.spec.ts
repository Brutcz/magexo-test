import { renderSuspended } from "@nuxt/test-utils/runtime";
import { expect, test } from "vitest";
import ProductPage from "~/pages/product/[slug].vue";

test("can render category page", async () => {
  const component = await renderSuspended(ProductPage, {
    route: {
      params: {
        slug: "carina-cardigan"
      }
    }
  });
  expect(component).toBeDefined();
});
