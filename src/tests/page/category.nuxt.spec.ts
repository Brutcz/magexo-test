import { renderSuspended } from "@nuxt/test-utils/runtime";
import { expect, test } from "vitest";
import CategoryPage from "~/pages/[category].vue";

test("can render category page", async () => {
  const component = await renderSuspended(CategoryPage, {
    route: {
      params: {
        category: "venia-tops"
      }
    }
  });
  expect(component).toBeDefined();
});
