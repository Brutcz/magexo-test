import vuetify, { transformAssetUrls } from "vite-plugin-vuetify";

export default defineNuxtConfig({
  srcDir: "src/",
  devtools: { enabled: true },
  build: {
    transpile: ["vuetify"]
  },
  modules: [
    "@nuxt/test-utils/module",
    "@nuxtjs/apollo",
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        // @ts-expect-error
        config.plugins.push(vuetify({ autoImport: true }));
      });
    }
  ],
  vite: {
    vue: {
      template: {
        transformAssetUrls
      }
    }
  },
  apollo: {
    clients: {
      default: {
        httpEndpoint: "https://venia.magento.com/graphql"
      }
    }
  }
});
